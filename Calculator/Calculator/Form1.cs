﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "0")
                richTextBox1_TextChanged("0");

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void richTextBox1_TextChanged(string text)
        {
            richTextBox1.Text = richTextBox1.Text + text;
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text.Length != 1)
            {
                StringBuilder builder = new StringBuilder(richTextBox1.Text);
                builder.Remove(richTextBox1.Text.Length - 1, 1);
                richTextBox1.Text = builder.ToString();
            }
            else richTextBox1.Text = "0";


        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text != "0")
            {
                richTextBox1.Clear();
            }
            richTextBox1.Text = "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("1");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("3");
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("4");
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("6");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("8");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Text == "0")
                richTextBox1.Clear();
            richTextBox1_TextChanged("9");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            bool temp = EndWith(richTextBox1.Text);
            if (!temp)
                richTextBox1_TextChanged(",");


        }

        private void button12_Click(object sender, EventArgs e)
        {
            bool temp = EndWith(richTextBox1.Text);
            if (!temp)
                richTextBox1_TextChanged("+");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            bool temp = EndWith(richTextBox1.Text);
            if (!temp)
                richTextBox1_TextChanged("-");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            bool temp = EndWith(richTextBox1.Text);
            if (!temp)
                richTextBox1_TextChanged("*");
        }

        private void button15_Click(object sender, EventArgs e)
        {
            bool temp = EndWith(richTextBox1.Text);
            if (!temp)
                richTextBox1_TextChanged("/");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            string item = richTextBox1.Text;
            StringBuilder builderr2 = new StringBuilder(item);
            string percent = "/100*";
            builderr2.Replace("%", percent);
            item = builderr2.ToString();
            StringBuilder builderr = new StringBuilder(item);
            builderr.Replace(",", ".");
            item = builderr.ToString();
            bool temp = EndWith(item);
            if (temp)
            {
                StringBuilder builder = new StringBuilder(item);
                builder.Remove(item.Length - 1, 1);
                item = builder.ToString();
            }
            string value = new DataTable().Compute(item, null).ToString();
            richTextBox1.Clear();
            richTextBox1_TextChanged(value);

        }

        private void button17_Click(object sender, EventArgs e)
        {
            string item = richTextBox1.Text;
            bool temp = EndWith(item);
            if (temp)
            {
                StringBuilder builder = new StringBuilder(item);
                builder.Remove(item.Length - 1, 1);
                item = builder.ToString();
            }
            richTextBox1_TextChanged("%");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            double root = Math.Sqrt(double.Parse(richTextBox1.Text));
            richTextBox1.Clear();
            richTextBox1_TextChanged(root.ToString());

        }
        private bool EndWith(string myString)
        {
            var arr = myString.ToCharArray();

            if (arr[arr.Length - 1].ToString() == "+")
            {
                return true;
            }
            if (arr[arr.Length - 1].ToString() == "-")
            {
                return true;
            }
            if (arr[arr.Length - 1].ToString() == "*")
            {
                return true;
            }
            if (arr[arr.Length - 1].ToString() == "/")
            {
                return true;
            }
            if (arr[arr.Length - 1].ToString() == "%")
            {
                return true;
            }
            if (arr[arr.Length - 1].ToString() == ",")
            {
                return true;
            }
            return false;

        }
    }
}
